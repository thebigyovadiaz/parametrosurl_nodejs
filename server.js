var express = require('express'),
	app = express(),
	port = 8888;

var parametros = [];

app.get('/', function(req, res){
	res.send('Probando cualquier texto');
});

app.get('/mensaje/:valor', function(req, res){
	parametros.push(req.params.valor);
});

app.get('/verMensaje', function(req, res){
	res.send('Recibiendo parámetro: ' + parametros +
		'<script>setTimeout(function(){window.location.reload()}, 2000)</script>');
});

app.listen(port);

console.log('Servidor corriendo en el puerto ' + port);
