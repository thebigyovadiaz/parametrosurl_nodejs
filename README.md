# Parámetros URL en NodeJS

	Ejemplo de como es el trabajo con parametros en NodeJS y Express.
	Se crea un servidor el cual a su vez en la url permite el ingreso de texto.

## Instalar

	Se baja el código, luego se hace ´npm install´ para las dependencias.
	Ejecutar el servidor con ´node server.js´
	Al estar en ejecución probar:
	
		* Ruta principal: localhost:8888
		* Ruta para tomar valor de parametro: localhost:8888/mensaje/loQueDesees
		* Ruta para ver parámetros: localhost:8888/verMensaje
		* Se recomienda probar la ruta que ve el mensaje y la que toma el parametro en pestañas diferentes para que veas mejor el funcionamiento

